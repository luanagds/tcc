package br.com.edu.ifrs.canoas.model;

import java.util.Date;

import android.R.integer;

public abstract class Registro {
	
	public double valor;
	public String observacao;
	public Categoria categoria;
	public int idcat;
	public String descTipo;
	public String descCat;
	public int tipo;
	public Date data;	
	public int id;
	private TipoRegistro tipoReg;
	
	public Registro() {}
		
	public Registro(double valor, String observacao, Categoria cat, Date data, TipoRegistro tipo) {
		this.valor = valor;
		this.observacao = observacao;
		this.idcat = cat.getId();
		this.descCat = cat.getDescricao();
		this.tipo = tipo.getId();
		this.descTipo = tipo.getDescricao();
		this.data = data;
	}
		
	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public TipoRegistro getTipoReg() {
		return tipoReg;
	}

	public void setTipoReg(TipoRegistro tipoReg) {
		this.tipoReg = tipoReg;
	}

	public String getDescTipo() {
		return descTipo;
	}

	public void setDescTipo(String descTipo) {
		this.descTipo = descTipo;
	}

	
	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	public String getDescCat() {
		return descCat;
	}

	public void setDescCat(String descCat) {
		this.descCat = descCat;
	}
	
	public int getIdcat() {
		return idcat;
	}

	public void setIdcat(int idcat) {
		this.idcat = idcat;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id++;
	}
		
	public double getValor() {
		if(tipo == 1){
			return -valor;
		}else if(tipo == 2){
			return valor;
		}
		else
			return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
		
	@Override
	public String toString() {
		/*return "{id:" +id+", valor:" + valor + ", observacao:" + observacao + ", categoria:" + idcat + ", desc:" + descCat+", data:" + data + ", id:"
				+ id + "tipo:"+tipo + "}";*/
		//return "{categoria:"+getDescCat()+",valor:"+valor+", observacao:"+observacao+", data:"+data+"}";
		return "{\"id\":\""+id+"\",\"valor\":\""+Math.abs(valor)+"\",\"observacao\":\""+observacao+"\",\"data\":\""+data+"\",\"idcat\":\""+idcat+"\",\"tipo\":\""+tipo+"\"}";
	}

}
