package br.com.edu.ifrs.canoas.model;

public class Categoria {
	private int id;
	private String descricao;
	//private double limite;
	private int desp = 0;
	private int rec = 0;
	
	public Categoria() {}
	
	public Categoria(String desc) {
		this.descricao = desc;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public int getId() {
		return id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setId(int id) {
		this.id = id;
	}

	/*public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}*/
	public void setDesp(int desp) {
		this.desp = desp;
	}
	public void setRec(int rec) {
		this.rec = rec;
	}
	public int getDesp() {
		return desp;
	}
	public int getRec() {
		return rec;
	}

	@Override
	public String toString() {
		//return "{\"id\":\""+id+"\",\"descricao\":\""+descricao+"\"}";
		return descricao;
		//return id+"-descricao:"+descricao+"-despesa:"+desp+"-receita:"+rec;
	}
	
	
}
