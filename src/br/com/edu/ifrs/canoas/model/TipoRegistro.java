package br.com.edu.ifrs.canoas.model;

public class TipoRegistro {
	private int id;
	private String descricao;
	
	public TipoRegistro() {}
	
	public TipoRegistro(String desc) {
		this.descricao = desc;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public int getId() {
		return id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return descricao;
	}	
}
