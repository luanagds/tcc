package br.com.edu.ifrs.canoas.model;

import java.sql.Date;

public class Notificacao {
	private int id;
	private Registro registro = new Despesa();
	private Date data;
	
	public Notificacao() {}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id++;
	}
	
	public Registro getRegistro() {
		return registro;
	}
	public void setRegistro(Registro registro) {
		this.registro = registro;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public Date getData() {
		return data;
	}

	@Override
	public String toString() {
		return "id = "+getId()+" idReg = "+getRegistro().getId()+" data = " + getData();
	}
}
