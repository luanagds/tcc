package br.com.edu.ifrs.canoas.model;

import java.util.Date;

public class Receita extends Registro {
		
	public Receita() {}

	public Receita(double valor, String observacao, Categoria cat, Date data, TipoRegistro tipo) {
		super(valor, observacao, cat, data, tipo);
	}
	
}
