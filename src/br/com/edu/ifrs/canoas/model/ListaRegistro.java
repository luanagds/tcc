package br.com.edu.ifrs.canoas.model;

import java.util.ArrayList;

public class ListaRegistro extends ArrayList<Registro>{

	private static final long serialVersionUID = 1L;
	private ArrayList<Despesa> listaDesp = new ArrayList<Despesa>();
	private ArrayList<Receita> listaRec = new ArrayList<Receita>();
    
    @Override
    public boolean add(Registro object) {
    	if(object.getClass().getCanonicalName().equals("br.com.edu.ifrs.canoas.model.Despesa")){
    	   listaDesp.add((Despesa) object);
    	}
    	else if (object.getClass().getCanonicalName().equals("br.com.edu.ifrs.canoas.model.Receita")){
			listaRec.add((Receita)object);
		}
    	return super.add(object);
    }    
	
    @Override
    public String toString() {
    	return listaDesp.toString();
    }
    	
}
