package br.com.edu.ifrs.canoas.model;

import java.util.Date;

public class Despesa extends Registro{
	
	//private double limite; -- onde colocar??
	
	public Despesa(){}

	public Despesa(double valor, String observacao, Categoria cat, Date data, TipoRegistro tipo) {		
		super(valor, observacao, cat, data, tipo);
	}
	
}
