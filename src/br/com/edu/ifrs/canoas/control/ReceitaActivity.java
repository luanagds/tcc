package br.com.edu.ifrs.canoas.control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.CategoriaDAO;
import br.com.edu.ifrs.canoas.dao.RegistroDAO;
import br.com.edu.ifrs.canoas.dao.TipoRegistroDAO;
import br.com.edu.ifrs.canoas.model.Categoria;
import br.com.edu.ifrs.canoas.model.Receita;
import br.com.edu.ifrs.canoas.model.Registro;
import br.com.edu.ifrs.canoas.model.TipoRegistro;

public class ReceitaActivity extends Activity {
	
	private static final int DATE_DIALOG_ID = 0;	
	private Spinner tipo;	
	private Button botaoData;	
	private EditText etValor, etObservacao;	
	private Registro r;	
	private int id;
	private ArrayAdapter<Categoria> adapter;
	private double valor;
	Calendar calendario = Calendar.getInstance(); 
	int ano = calendario.get(Calendar.YEAR); 
	int mes = calendario.get(Calendar.MONTH); 
	int dia = calendario.get(Calendar.DAY_OF_MONTH); 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.receitas);
		
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
				
		CategoriaDAO cd = new CategoriaDAO(this);	
		adapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_dropdown_item, cd.findCategRec());		
		
		tipo = (Spinner) findViewById(R.id.spnTipos);		

		tipo.setAdapter(adapter);
		
		tipo.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
				Categoria c = (Categoria)parent.getSelectedItem();
				id = c.getId();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		etValor = (EditText) findViewById(R.id.valorReceita);
		etObservacao = (EditText) findViewById(R.id.txtObsRec);	
		
		botaoData = (Button) findViewById(R.id.btnData);
		botaoData.setText(dia+"/"+(mes+1)+"/"+ano);
				
		botaoData.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(v == botaoData)
					showDialog(DATE_DIALOG_ID);				
			}
		});
	}
	
	public void salvaReceita(View v) throws ParseException {
		Categoria c = new Categoria();				
		String cat = String.valueOf(tipo.getSelectedItem());
		c.setDescricao(cat);
		c.setId(id);		
		
		String txtvalor = etValor.getText().toString();
		if(txtvalor.isEmpty())
			valor = 0;
		else
			valor = Double.parseDouble(txtvalor);
		String obs = String.valueOf(etObservacao.getText());
		if(obs  == null)
			obs = "";
		
		String data = String.valueOf(botaoData.getText());
		SimpleDateFormat forma = new SimpleDateFormat("dd/MM/yyyy");  
        java.sql.Date dt = new java.sql.Date(forma.parse(data).getTime());
    	TipoRegistroDAO td = new TipoRegistroDAO(this);
        TipoRegistro tp = new TipoRegistro();
		tp.setId(2);
		tp.setDescricao(td.findDesc(2));
        
		if(valor == 0)
			Toast.makeText(this, "Adicione valor!", Toast.LENGTH_SHORT).show();
		else{
			r = new Receita(valor, obs, c, dt, tp);        
			RegistroDAO dd = new RegistroDAO(this);
			dd.create(r);
			//dd.findRec();
			Toast.makeText(this, "Receita adicionada!", Toast.LENGTH_SHORT).show();	
			limpaTexto();
		}
	}
		
	public void limpaTexto() {
		tipo.setAdapter(adapter);
		etValor.setText(String.valueOf(""));
		etObservacao.setText("");
		botaoData.setText(dia+"/"+(mes+1)+"/"+ano); 
		etValor.requestFocus();
	}
	
	//calendario
		@Override 
		protected Dialog onCreateDialog(int id) {
						
			switch (id) { 
			
				case DATE_DIALOG_ID: 
					return new DatePickerDialog(this, mDateSetListener, ano, mes, dia); 
			} 
			return null; 
		} 
		
		private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) { 
				String dia = String.valueOf(dayOfMonth);
				String mes = String.valueOf(monthOfYear+1);
				
				String data = dia + "/" + mes + "/" + String.valueOf(year);
				botaoData.setText(data);
			}		
		};
}
