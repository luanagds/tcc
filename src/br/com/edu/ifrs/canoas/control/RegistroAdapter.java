package br.com.edu.ifrs.canoas.control;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.model.Registro;

public class RegistroAdapter extends BaseAdapter {
	
	private Context context;
	private List<Registro> lista;
	
	public RegistroAdapter(List<Registro>lista, Context context){
		this.lista = lista;
		this.context = context;		
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return lista.get(position).getId();
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		final int auxPosition = position;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.lista_registros, null);
						
		TextView reg = (TextView)layout.findViewById(R.id.tvReg);
		TextView val = (TextView)layout.findViewById(R.id.tvValor);
		TextView tvdata = (TextView)layout.findViewById(R.id.tvDataReg);
		
		reg.setText(String.valueOf(lista.get(position).getDescCat()));		

		String ret = String.valueOf(lista.get(position).getValor());
		String ret2 = ret.replaceAll("-", "");
				
		val.setText("R$"+ret2);
					
		Date data = (Date) lista.get(position).getData(); 
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
		String dataFormatada = formato.format(data); 
		
		tvdata.setText(dataFormatada);	
				
		return layout;		
	}

}
