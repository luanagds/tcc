package br.com.edu.ifrs.canoas.control;

import java.util.Calendar;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.NotificacaoDAO;
import br.com.edu.ifrs.canoas.dao.RegistroDAO;
import br.com.edu.ifrs.canoas.dao.TipoRegistroDAO;
import br.com.edu.ifrs.canoas.model.Despesa;
import br.com.edu.ifrs.canoas.model.Notificacao;
import br.com.edu.ifrs.canoas.model.Registro;
import br.com.edu.ifrs.canoas.model.TipoRegistro;

public class RelatorioActivity extends Activity {
	
	private long retid=1;
	private ListView listRel;
	private TextView tvempty;
	private Calendar c = Calendar.getInstance();
	private int mes = c.get(Calendar.MONTH);	
	private int ano = c.get(Calendar.YEAR);
	private RegistroDAO regDao;
	private List<Registro> list;	
	private long id;
	
	public void onCreate(Bundle savedInstanceState) {
		Spinner tipo;

		super.onCreate(savedInstanceState);
		setContentView(R.layout.relatorio);
		mes++;
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
				
		TipoRegistroDAO td = new TipoRegistroDAO(this);	
		ArrayAdapter<TipoRegistro> adapter = new ArrayAdapter<TipoRegistro>(this, android.R.layout.simple_spinner_dropdown_item, td.findAll());		
						
		tipo = (Spinner) findViewById(R.id.spTipos);		

		tipo.setAdapter(adapter);
				
		listRel = (ListView) findViewById(R.id.lvRel);
		regDao = new RegistroDAO(this);	
		
		tvempty = (TextView)findViewById(R.id.idEmpty);
		
		tipo.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id2) {				
				
				RadioButton m = (RadioButton)findViewById(R.id.rdMensal);
				RadioButton a = (RadioButton)findViewById(R.id.rdAnual);
				TipoRegistro tp = (TipoRegistro)parent.getSelectedItem();
				id = tp.getId();
				retornaId(id);
				if(m.isChecked()){
					list = regDao.findReg(id, mes);
				}else if(a.isChecked()){
					list = regDao.findRegAn(id, ano);
				}
				if(list.isEmpty()){	
					list.clear();
					listRel.setAdapter(new RegistroAdapter(list, RelatorioActivity.this));
					tvempty.setText("N�o h� registros para exibir!");					
				}else{
					tvempty.setText("");
					listRel.setAdapter(new RegistroAdapter(list, RelatorioActivity.this));
				}
			}			

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		RadioGroup rg = (RadioGroup)findViewById(R.id.grupo);
		rg.setOnCheckedChangeListener(new OnCheckedChangeListener(){
		        @Override
		        public void onCheckedChanged(RadioGroup group, int checkedId) {
		        	RadioButton m = (RadioButton)findViewById(R.id.rdMensal);
					RadioButton a = (RadioButton)findViewById(R.id.rdAnual);
					if(m.isChecked()){
						list = regDao.findReg(retid, mes);
						listRel.setAdapter(new RegistroAdapter(list, RelatorioActivity.this));
		        }
					else if(a.isChecked()){
						list = regDao.findRegAn(retid, ano);
						listRel.setAdapter(new RegistroAdapter(list, RelatorioActivity.this));
					}
		        }
			});
		
			listRel.setOnItemClickListener(new OnItemClickListener(){
	
				@Override
				public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
					AlertDialog alerta;
					
					Log.v("MSG", list.get(position).toString());
					AlertDialog.Builder builder = new AlertDialog.Builder(RelatorioActivity.this); 
				
					builder.setNegativeButton("Excluir registro", new DialogInterface.OnClickListener() { 
						public void onClick(DialogInterface arg0, int arg1) {	
							Registro d = new Despesa();
							d = list.get(position);
							RegistroDAO rd = new RegistroDAO(RelatorioActivity.this);
							//NotificacaoDAO nd = new NotificacaoDAO(getApplicationContext());
							//Notificacao n = new Notificacao();
							
							rd.remove(d);
							//nd.remove(n);
							Toast.makeText(getApplicationContext(), "Registro exclu�do!", Toast.LENGTH_LONG).show();
							list = regDao.findRegAn(retid, ano);
							if(list.isEmpty()){	
								list.clear();
								listRel.setAdapter(new RegistroAdapter(list, RelatorioActivity.this));
								tvempty.setText("N�o h� registros para exibir!");					
							}else{
								tvempty.setText("");
								listRel.setAdapter(new RegistroAdapter(list, RelatorioActivity.this));
							}
						}
					});		 	
					//cria o AlertDialog 
					alerta = builder.create(); 
					//Exibe 
					alerta.show();
				}	
			});		
	}	
	
	public void retornaId(long id) {

		Log.v("Id", String.valueOf(id));
		retid = id;
	}
	

}
