package br.com.edu.ifrs.canoas.control;

import java.util.Calendar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.CategoriaDAO;
import br.com.edu.ifrs.canoas.dao.RegistroDAO;
import br.com.edu.ifrs.canoas.service.NotificacaoServico;

public class MainActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	
		
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
              
        Intent intent = new Intent("POUPE_SERVICE");
		startService(intent);	
		this.startService(new Intent(this, NotificacaoServico.class));
        informaRegistros();
        
        RegistroDAO rd = new RegistroDAO(this);
        rd.findAll();
        
        CategoriaDAO cd = new CategoriaDAO(this);
        cd.findAll();
		
	}
	
	public void informaRegistros() {
		
		TextView desp, rec, total, totalAno;

		Calendar calendario = Calendar.getInstance(); 
		int mes = calendario.get(Calendar.MONTH); 
		int ano = calendario.get(Calendar.YEAR);
		
		mes++;
		
	    TextView tvmes = (TextView)findViewById(R.id.tvmesAtual);	   
	   
        tvmes.setText("M�s: "+exibeMesAtual(mes));
	        
		RegistroDAO rd = new RegistroDAO(this);
						
		desp = (TextView) findViewById(R.id.tvTotDesp);
		String ret = rd.buscaDesp(mes, ano);
		String ret2 = ret.replaceAll("-", "");
		desp.setText("R$"+ret2);
		
		rec = (TextView) findViewById(R.id.tvTotRec);		
		rec.setText("R$"+rd.buscaRec(mes, ano));
		
		total = (TextView) findViewById(R.id.tvTotal);
		ret = rd.buscaTotal(mes, ano);
		if(ret.contains("-"))
			total.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
			
		total.setText("R$"+ret);
		
		totalAno = (TextView) findViewById(R.id.tvTotAno);	
		ret = rd.buscaTotalAno(ano);
		if(ret.contains("-"))
			totalAno.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
		totalAno.setText("R$"+ret);
	}
	
	public void addDespesa(View v){
		startActivity(new Intent(this, DespesasActivity.class));
	}
	
	public void addReceita(View v){
		startActivity(new Intent(this, ReceitaActivity.class));
	}
	
	public void verGrafico(View v){
		startActivity(new Intent(this, GraficoActivity.class));
	}
	
	public void verRelatorio(View v){
		startActivity(new Intent(this, RelatorioActivity.class));
	}
	
	public void exibeOpcoes(View v){
		startActivity(new Intent(this, OpcoesActivity.class));		
	}
	
	
	public void mudaCor(View v) {
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_blue_dark));
        actionBar.setBackgroundDrawable(color);        
	}
	
	public void botaoSair(View v) {
		
		AlertDialog alerta;
		
		//Cria o gerador do AlertDialog 
		AlertDialog.Builder builder = new AlertDialog.Builder(this); 
		//define o titulo
		builder.setTitle("Sair"); 
		//define a mensagem
		builder.setMessage("Tem certeza que deseja sair?"); 
		//define um bot�o como positivo 
		builder.setNegativeButton("Sair", new DialogInterface.OnClickListener() { 
			public void onClick(DialogInterface arg0, int arg1) {				
				finish();
			}
		});
		//define um bot�o como negativo. 
		builder.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) { 
				Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
			}
		}); 		 	
		//cria o AlertDialog 
		alerta = builder.create(); 
		//Exibe 
		alerta.show();
	}
	
	public String exibeMesAtual(int mes) {
		String mesatual = null;
		
		switch(mes){
			case 1: mesatual = "Janeiro";
				break;
			case 2: mesatual = "Fevereiro";
				break;
			case 3: mesatual = "Mar�o";
				break;
			case 4: mesatual = "Abril";
				break;
			case 5: mesatual = "Maio";
				break;
			case 6: mesatual = "Junho";
				break;
			case 7: mesatual = "Julho";
				break;
			case 8: mesatual = "Agosto";
				break;
			case 9: mesatual = "Setembro";
				break;
			case 10: mesatual = "Outubro";
				break;
			case 11: mesatual = "Novembro";
				break;
			case 12: mesatual = "Dezembro";
		}
		return mesatual;
	}
	
	
}
