package br.com.edu.ifrs.canoas.control;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.CategoriaDAO;
import br.com.edu.ifrs.canoas.model.Categoria;

public class CategoriaActivity extends Activity{

	EditText etCat;
	Categoria c ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.categoria);
		
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
		
	}

	public void salvaCategoria(View v) {
		c = new Categoria();
		etCat = (EditText) findViewById(R.id.descricao);
		String cat = String.valueOf(etCat.getText()).toUpperCase();
		if(!cat.isEmpty()){	
			CategoriaDAO cd = new CategoriaDAO(this);
			int val = cd.findCategoria(cat);
			if(val == 1){
				Toast.makeText(this, "Essa categoria j� existe!", Toast.LENGTH_SHORT).show();
				etCat.setText("");
			}else{
				CheckBox cbdesp = (CheckBox)findViewById(R.id.cbDesp);
				CheckBox cbrec = (CheckBox)findViewById(R.id.cbRec);
				
				if(cbdesp.isChecked()){
					c.setDesp(1);
				}else{
					c.setDesp(0);}
				if(cbrec.isChecked()){
					c.setRec(1);
				}else{
					c.setRec(0);
			}
				c.setDescricao(cat);				
				cd.create(c);			
				etCat.setText("");
				Toast.makeText(this, "Categoria inserida!", Toast.LENGTH_SHORT).show();
				cd.findAll();
			}
		}else
			Toast.makeText(this, "Insira um valor v�lido!", Toast.LENGTH_SHORT).show();
	}
}
