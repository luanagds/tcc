package br.com.edu.ifrs.canoas.control;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.RegistroDAO;
import br.com.edu.ifrs.canoas.dao.TipoRegistroDAO;
import br.com.edu.ifrs.canoas.model.TipoRegistro;

public class GraficoActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.graficos);
		
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
		
        TipoRegistroDAO td = new TipoRegistroDAO(this);	
		ArrayAdapter<TipoRegistro> adapter = new ArrayAdapter<TipoRegistro>(this, android.R.layout.simple_spinner_dropdown_item, td.findAll());		
						
		Spinner tipo = (Spinner) findViewById(R.id.spngraf);		

		tipo.setAdapter(adapter);
		
		tipo.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				// TODO Auto-generated method stub
				TipoRegistro tp = (TipoRegistro)parent.getSelectedItem();
				int tipo = tp.getId();
				Log.v("TIPO", String.valueOf(tipo));
		        exibeGrafico(tipo);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
				         
	}
	
	public void exibeGrafico(int tipo) {

		Calendar calendario = Calendar.getInstance(); 
		int ano = calendario.get(Calendar.YEAR);
		WebView wvGrafico;
		String strURL;		
		double total;
		String var = "";
		double tot = 0;
		String var2 = "";
		String nomes = "";
		String aux = "";
		
		RegistroDAO rd = new RegistroDAO(this);       
		List<Double>valorcategorias = new ArrayList<Double>();
        //buscar categoria de despesas
		List<String> listacategorias = new ArrayList<String>();
		 
		listacategorias = rd.buscaCategReg(tipo);
        
        for(int i = 0; i<listacategorias.size();i++){
        	nomes+=listacategorias.get(i);
        	aux = listacategorias.get(i);
        	if(i!=(listacategorias.size()-1)){
        		nomes = nomes+"|";
        	}
        	valorcategorias.add(rd.buscaTodosReg(aux, tipo));
        }
        
        total = Math.abs(rd.buscaTotReg(ano, tipo));
        
        for(int i = 0; i < valorcategorias.size();i++){
        	tot = Math.round((valorcategorias.get(i)*100)/total);
        	var += String.valueOf(tot);
        	var2 += String.valueOf(tot);
        	if(i!=(valorcategorias.size()-1)){
        		var+=",";
        		var2+="%|";
        	}else
        		var2+="%";
        }
        strURL = "https://chart.googleapis.com/chart?"
        		+ "cht=p&"
        		+ "chs=450x240&"
        		+ "chd=t:"+var+"&"
        		+ "chl="+var2+"&"
        		+ "chco=0A8C8A,EBB671&"
        		+ "chtt=Valores do ano "+ano+"&"
        		+ "chdl="+nomes+"&";
	         
        wvGrafico = (WebView)findViewById(R.id.webView1);
        wvGrafico.loadUrl(strURL);
	}
}
