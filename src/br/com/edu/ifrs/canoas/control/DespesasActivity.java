package br.com.edu.ifrs.canoas.control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.CategoriaDAO;
import br.com.edu.ifrs.canoas.dao.NotificacaoDAO;
import br.com.edu.ifrs.canoas.dao.RegistroDAO;
import br.com.edu.ifrs.canoas.dao.TipoRegistroDAO;
import br.com.edu.ifrs.canoas.model.Categoria;
import br.com.edu.ifrs.canoas.model.Despesa;
import br.com.edu.ifrs.canoas.model.Notificacao;
import br.com.edu.ifrs.canoas.model.Registro;
import br.com.edu.ifrs.canoas.model.TipoRegistro;

public class DespesasActivity extends Activity {	
	
	private EditText etValor, etObservacao; 
	private Spinner tpoDespesa;		
	private Button botaoData;	
	private static final int DATE_DIALOG_ID = 0;
	private int id;
	private ArrayAdapter<Categoria> adapter;
	
	private Calendar dataAtual = Calendar.getInstance();
	int ano = dataAtual.get(Calendar.YEAR); 
	int mes = dataAtual.get(Calendar.MONTH); 
	int dia = dataAtual.get(Calendar.DAY_OF_MONTH); 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.despesas);
		
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
				
		CategoriaDAO cd = new CategoriaDAO(this);	
		adapter = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_dropdown_item, cd.findCategDes());		
		
		tpoDespesa = (Spinner) findViewById(R.id.spnTipos);		

		tpoDespesa.setAdapter(adapter);
	
		tpoDespesa.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View arg1, int arg2, long arg3) {
				Categoria c = (Categoria)parent.getSelectedItem();
				id = c.getId();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});		
		
		//encontra campos de texto
		etValor = (EditText) findViewById(R.id.vlrDespesa);
		etObservacao = (EditText) findViewById(R.id.txtObsDesp);				
		
		botaoData = (Button) findViewById(R.id.btnData);
		botaoData.setText(dia+"/"+(mes+1)+"/"+ano);
				
		botaoData.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(v == botaoData)
					showDialog(DATE_DIALOG_ID);				
			}
		});
		
	}
	
	public void salvaDespesa(View v) throws ParseException {

		Registro d;	
		double valor;
		String obs;
		String data;
		
		Categoria c = new Categoria();
		RegistroDAO dd = new RegistroDAO(this);
			
		String cat = String.valueOf(tpoDespesa.getSelectedItem());
		c.setDescricao(cat);
		c.setId(id);

		String txtValor = (etValor.getText().toString());
		
		if(txtValor.isEmpty())
			valor = 0;
		else
			valor = Double.parseDouble(txtValor);
		obs = etObservacao.getText().toString();
		if(obs == null){
			 obs = "";
		}
		
		data = botaoData.getText().toString();		
		
		SimpleDateFormat forma = new SimpleDateFormat("dd/MM/yyyy");  
        java.sql.Date dt = new java.sql.Date(forma.parse(data).getTime()); 
        
		TipoRegistroDAO td = new TipoRegistroDAO(this);
		
		TipoRegistro tp = new TipoRegistro();
		tp.setId(1);
		tp.setDescricao(td.findDesc(1));
			
		if(valor == 0){			
			Toast.makeText(this, "Adicione valor!", Toast.LENGTH_SHORT).show();		    
		}else{
		
			d = new Despesa(valor, obs, c, dt, tp);		
			dd.create(d);//add no banco		
		
			CheckBox not = (CheckBox)findViewById(R.id.cbNotifica);
			if (not.isChecked()){
				agendarNotificacao();
				Log.v("NOTIFICA��O", dt.toString());
				Notificacao n = new Notificacao();
				n.setData(dt);
				n.setRegistro(d);
				NotificacaoDAO nd = new NotificacaoDAO(this);
				nd.create(n);
				nd.findAll();
			}
			
			Toast.makeText(this, "Despesa adicionada!", Toast.LENGTH_SHORT).show();	
						
			limpaTexto();
		}
		Intent intent = getIntent();
		
		if(intent != null){
			Bundle params = intent.getExtras();
			if(params != null && params.getInt("id") != 1){
				finish();//verificar finish fragment
			}
		}
		
	}
	
	public void limpaTexto() {
		CheckBox not = (CheckBox)findViewById(R.id.cbNotifica);
		not.setChecked(false);
		tpoDespesa.setAdapter(adapter);
		etValor.setText(String.valueOf(""));
		etObservacao.setText("");
		botaoData.setText(dia+"/"+(mes+1)+"/"+ano); 
		etValor.requestFocus();
	}

	//calendario
	@Override 
	protected Dialog onCreateDialog(int id) {
		
		Calendar calendario = Calendar.getInstance(); 
		int ano = calendario.get(Calendar.YEAR); 
		int mes = calendario.get(Calendar.MONTH); 
		int dia = calendario.get(Calendar.DAY_OF_MONTH); 
		
		switch (id) { 
		
		case DATE_DIALOG_ID: 
			return new DatePickerDialog(this, mDateSetListener, ano, mes, dia); 
		} 
		return null; 
	} 
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) { 
			String dia = String.valueOf(dayOfMonth);
			String mes = String.valueOf(monthOfYear+1);
		
			String data = dia + "/" + mes + "/" + String.valueOf(year);
			
			botaoData.setText(data);
		}		
	};	
	
	public void agendarNotificacao() {
		NotificationManager nm = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);
		nm.cancel(001);
	}
}
