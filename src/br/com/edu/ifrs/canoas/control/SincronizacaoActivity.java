package br.com.edu.ifrs.canoas.control;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.dao.CategoriaDAO;
import br.com.edu.ifrs.canoas.dao.RegistroDAO;
import br.com.edu.ifrs.canoas.model.Categoria;
import br.com.edu.ifrs.canoas.model.Despesa;
import br.com.edu.ifrs.canoas.model.ListaRegistro;
import br.com.edu.ifrs.canoas.model.Registro;

public class SincronizacaoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sincronizacao);
		
		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
        
        enviaDadosWeb();
       
	}
	
	private void enviaDadosWeb(){
		final Button b = (Button) findViewById(R.id.btnSincr);
		b.setEnabled(false);
		b.setOnClickListener(
			new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					RegistroDAO rd = new RegistroDAO(SincronizacaoActivity.this);
					List lista = new ListaRegistro();
					
					lista = rd.findAll();
					
					EditText tvLogin = (EditText)findViewById(R.id.editText2);
					EditText tvSenha = (EditText)findViewById(R.id.editText1);
					String login = String.valueOf(tvLogin.getText());
					String senha = String.valueOf(tvSenha.getText());
					
					String user = "[{\"usuario\":\""+login+"\",\"senha\":\""+senha+"\"}]";
					try{
					    new DownloadWebpageTask().execute("http://webacademico.canoas.ifrs.edu.br/~luana.silva/recebeDados.php?lista="+lista);
					    AlertDialog.Builder builder = new AlertDialog.Builder(SincronizacaoActivity.this); 				
						builder.setMessage("Dados sincronizados!"); 
						builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) { 
								
							}
						});
						builder.show();
					} catch (Exception  e) {
					    Log.e("Deu problema", "Exce��o ");
					}	
				}
			});
		final Button bb = (Button) findViewById(R.id.btnUser);
		
		bb.setOnClickListener(new View.OnClickListener() {		
			
			@Override
			public void onClick(View v) {
				EditText tvLogin = (EditText)findViewById(R.id.editText2);
				EditText tvSenha = (EditText)findViewById(R.id.editText1);
				String login = String.valueOf(tvLogin.getText());
				String senha = String.valueOf(tvSenha.getText());
				
				String user = "[{\"usuario\":\""+login+"\",\"senha\":\""+senha+"\"}]";
						
				Log.v("usuario:", user);
				new DownloadWebpageTask().execute("http://webacademico.canoas.ifrs.edu.br/~luana.silva/recebeDados2.php?user="+user);
				bb.setEnabled(false);
				b.setEnabled(true);
			}
		});				
	}	
	
	private String downloadUrl(String myurl) throws IOException {
	    InputStream is = null;
	    // Only display the first 500 characters of the retrieved
	    // web page content.
	    int len = 500;
	        
	    try {
	        URL url = new URL(myurl);
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	        conn.setReadTimeout(10000 /* milliseconds */);
	        conn.setConnectTimeout(15000 /* milliseconds */);
	        conn.setRequestMethod("POST");
	        conn.setDoInput(true);
	        // Starts the query
	        conn.connect();
	        int response = conn.getResponseCode();
	        Log.d("LOL", "The response is: " + response);
	        is = conn.getInputStream();
	        // Convert the InputStream into a string
	        String contentAsString = readIt(is, len);
	        return contentAsString;
	        
	    // Makes sure that the InputStream is closed after the app is
	    // finished using it.
	    } finally {
	        if (is != null) {
	            is.close();
	        } 
	    }
	}	
	
	public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
	    Reader reader = null;
	    reader = new InputStreamReader(stream, "UTF-8");        
	    char[] buffer = new char[len];
	    reader.read(buffer);
	    return new String(buffer);
	}

	//Para ler o JSON com biblioNativa
	//http://developer.android.com/reference/android/util/JsonReader.html
	//Processando JSON com libJava
	public String processaJson(String json){
		Toast.makeText(getApplicationContext(), "Processando resultados", Toast.LENGTH_LONG).show();
	//	PrecoCarne precoCarne = new PrecoCarne();
		Registro reg = new Despesa();
		RegistroDAO rd = new RegistroDAO(this);
		List<Registro>lista = new ArrayList<Registro>();
		lista = rd.findAll();
		try {
			JSONObject jsonObjeto = new JSONObject(String.valueOf(lista));
		//	precoCarne.setPrecoMinimo(jsonObjeto.getLong("precoMinimo"));
			reg.setId(jsonObjeto.getInt("id"));
			
		} catch (JSONException e) {
			Toast.makeText(getApplicationContext(), "Resposta invalida", Toast.LENGTH_LONG).show();
		}
		
		return reg.toString();//precoCarne.toString();
	}
	
	
	private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {              
            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	Log.v("result", result);
    		
       }
    }
	
	
}
