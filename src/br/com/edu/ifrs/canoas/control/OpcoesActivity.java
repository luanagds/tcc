package br.com.edu.ifrs.canoas.control;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import br.com.edu.ifrs.canoas.R;

public class OpcoesActivity extends Activity {
	
	Button btnAddTipoDesp, btnAddTipoRenda;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.opcoes);

		final ActionBar actionBar = getActionBar();
        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_purple));
        actionBar.setBackgroundDrawable(color);
				
		btnAddTipoDesp = (Button)findViewById(R.id.btnCat);
		btnAddTipoDesp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(OpcoesActivity.this, CategoriaActivity.class));	
				
			}
		});
		
	}
	
    public void enviaDadosWeb(View v) {
		startActivity(new Intent(this, SincronizacaoActivity.class));
	}

//	public void mudaCor(View v) {
//		final ActionBar actionBar = getActionBar();
//        Drawable color = new ColorDrawable(getResources().getColor(android.R.color.holo_blue_dark));
//        actionBar.setBackgroundDrawable(color);
//       // Button b = (Button)findViewById(R.id.btnMudaCor);
//        b.setBackground(new ColorDrawable(getResources().getColor(android.R.color.holo_blue_light)));
//        b = (Button)findViewById(R.id.btnCat);
//        b.setBackground(new ColorDrawable(getResources().getColor(android.R.color.holo_blue_light)));
//	}
	
}
