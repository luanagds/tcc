package br.com.edu.ifrs.canoas.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Contacts.SettingsColumns;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import br.com.edu.ifrs.canoas.R;
import br.com.edu.ifrs.canoas.control.DespesasActivity;
import br.com.edu.ifrs.canoas.control.RelatorioActivity;
import br.com.edu.ifrs.canoas.dao.NotificacaoDAO;
import br.com.edu.ifrs.canoas.model.Notificacao;

public class NotificacaoServico extends Service{
	
	private List<Notificacao> listNotificacao = new ArrayList<Notificacao>();
	private NotificationManager mNotifyMgr;
	private NotificacaoDAO nd;

	private TimerTask timeTask = new TimerTask() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(true){
				try {
					Thread.sleep(1000*60*5);//60*60*24);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				//Toast.makeText(ct, "Disparando Broadcast", Toast.LENGTH_SHORT).show();
				notifica();
			}
		}
	};
	/*private Handler handler = new Handler(){
		public void handler
	}*/
	@Override
	public IBinder onBind(Intent intent) {
		
		return null;
	}
	
	@Override
	public void onCreate() {
		nd = new NotificacaoDAO(this);		
		mNotifyMgr = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);		
	}	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		Log.i("POUPE", "onStartCommand()");
		
		new Thread(timeTask).start();	
		
		return(super.onStartCommand(intent, flags, startId));
	}
	
	public void notifica() {
		
			Calendar horaAtual = Calendar.getInstance(); 
			int hora = horaAtual.get(Calendar.HOUR_OF_DAY);
			int min = horaAtual.get(Calendar.MINUTE);	
			int dia = horaAtual.get(Calendar.DAY_OF_MONTH);
			int mes = horaAtual.get(Calendar.MONTH);
			mes++;
			int ano = horaAtual.get(Calendar.YEAR);
			
			if(nd.buscaRegistro(dia, mes, ano)> 0){
				
				// Gets an instance of the NotificationManager service				
				Intent resultIntent = new Intent(this, RelatorioActivity.class);

				PendingIntent resultPendingIntent = PendingIntent.getActivity( // atividade
						this, // Contexto
						0, // Parametro n�o usado
						resultIntent, // Intent que lancará
						PendingIntent.FLAG_UPDATE_CURRENT
						);
				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
						this).setSmallIcon(R.drawable.pmais)
						.setContentTitle("Notifica��o Despesa")
						.setContentText("Voc� possui "+nd.buscaRegistro(dia, mes, ano)+" despesa(s) hoje!")
						.setTicker("Despesa");

				// Sets an ID for the notification
				int mNotificationId = 002;

				NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
				String [] descs = new String[]{"Data:"+dia+"/"+mes+"/"+ano+"", "Voc� possui "+nd.buscaRegistro(dia, mes, ano)+" despesa(s) hoje!"};
				for(int i = 0; i < descs.length; i++){
					style.addLine(Html.fromHtml(descs[i]));
				}
				mBuilder.setStyle(style);
				mBuilder.setNumber(descs.length);			

				mBuilder.setContentIntent(resultPendingIntent);// Seta a intent que vai
																// abrir

				Notification n = mBuilder.build();
				n.flags = Notification.FLAG_AUTO_CANCEL;
				mNotifyMgr.notify(mNotificationId, n);
				
			}
		//}
	}
	
}

		
