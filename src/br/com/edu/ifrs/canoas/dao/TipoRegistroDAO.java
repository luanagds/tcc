package br.com.edu.ifrs.canoas.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.com.edu.ifrs.canoas.model.TipoRegistro;

public class TipoRegistroDAO extends AbstractDAO<TipoRegistro>{

	public TipoRegistroDAO(Context t){
		iniciaDB(t);
	}
	
	@Override
	public boolean create(TipoRegistro obj) {
		ContentValues cv = new ContentValues();
		cv.put("descricao", obj.getDescricao());
		db.insert("TIPOREGISTRO", null, cv);
		return true;
	}

	@Override
	public int update(TipoRegistro obj) {
		ContentValues cv = new ContentValues();
		cv.put("descricao", obj.getDescricao());
		return db.update("TIPOREGISTRO", cv, "_id = ?", new String[]{""+obj.getId()});
	}
		
	@Override
	public int remove(TipoRegistro obj) {
		return db.delete("TIPOREGISTRO", "_id = "+obj.getId(), null);
	}
		
	public String findDesc(int id){
		String desc = null;
		String[] campos= {"descricao"};			
		Cursor c = db.query("tiporegistro", campos, "_id = "+id, 
										null, null, 
										null, null);		
		if(c.moveToFirst()){		
			TipoRegistro tr = new TipoRegistro();
			tr.setDescricao(c.getString(0));
			desc = tr.getDescricao();
		}			
		return desc;
	}
		
	@Override
	public List<TipoRegistro> findAll() {
		List<TipoRegistro>lista = new ArrayList<TipoRegistro>();
		String[] campos= {"_id","descricao"};			
		Cursor c = db.query("tiporegistro", campos, null, 
										null, null, 
										null, "descricao ASC");			
		if(c.moveToFirst()){
			do{
				TipoRegistro tr = new TipoRegistro();
				tr.setId(c.getInt(0));
				tr.setDescricao(c.getString(1));
				lista.add(tr);
			}while(c.moveToNext());
		}			
		return lista;		
	}

}
