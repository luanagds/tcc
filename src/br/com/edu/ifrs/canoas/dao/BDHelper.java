package br.com.edu.ifrs.canoas.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDHelper extends SQLiteOpenHelper {	

	private final static String NOMEBANCO = "myApp";
	private final static int VERSAO = 55;

	public BDHelper(Context context) {
		super(context, NOMEBANCO, null, VERSAO);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("CREATE TABLE TIPOREGISTRO(" 
				+" _id INTEGER primary KEY autoincrement,"
				+" descricao text not null);");
		
		db.execSQL("CREATE TABLE CATEGORIA("
				+ "	_id INTEGER primary KEY autoincrement,"
				+ " descricao text not null,"
				+ " despesa INTEGER,"
				+ " receita INTEGER);");
		
		db.execSQL("CREATE TABLE REGISTRO(" 
				+" _id INTEGER primary KEY autoincrement,"
				+" valor REAL not null,"
				+" observacao text not null,"
				+" tipo INTEGER not null,"
				+" cat INTEGER not null,"
				+" data text not null,"
				+" FOREIGN KEY(tipo) REFERENCES TIPOREGISTRO(_id),"
				+" FOREIGN KEY(cat) REFERENCES CATEGORIA(_id));");			
		
		db.execSQL("CREATE TABLE USUARIO("
				+ "	_id INTEGER primary KEY autoincrement,"
				+ " nome text not null,"
				+ " login text not null, "
				+ " senha text not null);");
		
		db.execSQL("CREATE TABLE NOTIFICACAO("
				+ " _id INTEGER primary KEY autoincrement,"
				+ " idReg INTEGER not null,"
				+ " data text not null,"
				+ " FOREIGN KEY(idReg) REFERENCES REGISTRO(_id));");
		
		db.execSQL("INSERT INTO CATEGORIA (descricao, despesa, receita) VALUES ('SALARIO', 0, 1)");
		db.execSQL("INSERT INTO CATEGORIA (descricao, despesa, receita) VALUES ('GORJETA', 0, 1)");
		db.execSQL("INSERT INTO CATEGORIA (descricao, despesa, receita) VALUES ('CONTA DE LUZ', 1, 0)");
		
		db.execSQL("INSERT INTO TIPOREGISTRO (descricao) VALUES ('DESPESA')");
		db.execSQL("INSERT INTO TIPOREGISTRO (descricao) VALUES ('RECEITA')");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS REGISTRO");
		db.execSQL("DROP TABLE IF EXISTS TIPOREGISTRO");
		db.execSQL("DROP TABLE IF EXISTS USUARIO");
		db.execSQL("DROP TABLE IF EXISTS CATEGORIA");
		db.execSQL("DROP TABLE IF EXISTS NOTIFICACAO");
        onCreate(db);		
	}

}
