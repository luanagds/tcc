package br.com.edu.ifrs.canoas.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.edu.ifrs.canoas.model.Notificacao;

public class NotificacaoDAO extends AbstractDAO<Notificacao>{

	public NotificacaoDAO(Context t){
		iniciaDB(t);
	}
	
	@Override
	public boolean create(Notificacao obj) {
		ContentValues cv = new ContentValues();
		cv.put("idReg", obj.getRegistro().getId());
		cv.put("data", String.valueOf(obj.getData().toString()));
		db.insert("notificacao", null, cv);
		return true;
	}

	@Override
	public int update(Notificacao obj) {
		ContentValues cv = new ContentValues();
		cv.put("idReg", obj.getRegistro().getId());
		return db.update("notificacao", cv, "_id = ?", new String[]{""+obj.getId()});
	}

	@Override
	public int remove(Notificacao obj) {
		return db.delete("notificacao", "_id = "+obj.getId(), null);
	}
	
	
	@Override
	public List<Notificacao> findAll() {
		List<Notificacao>lista = new ArrayList<Notificacao>();
		String[] campos= {"_id", "idReg", "data"};			
		Cursor c = db.query("Notificacao", campos, null, 
										null, null, 
										null, "data DESC");
		if(c.moveToFirst()){
			do{
				Notificacao cat = new Notificacao();
				cat.setId(c.getInt(0));
				cat.getRegistro().setId(c.getInt(1));
				cat.setData(Date.valueOf(c.getString(2)));
				lista.add(cat);
			}while(c.moveToNext());
			Log.v("lista notificação:", lista.toString());
		}			
		return lista;		
	}

	public int buscaRegistro(int dia, int mes, int ano){
		
		String diaa=String.valueOf(dia);
		
		if (dia<10) {
			diaa = "0"+dia;
		}
		Log.v("DIA", diaa);
		Cursor c = db.rawQuery("select count(data) from notificacao where "
				+ " strftime('%m', data) = '"+mes+"' and strftime('%Y', data) = '"+ano+"' and strftime('%d', data) = '"+diaa+"'", null);
		if(c.moveToFirst()){			
			return c.getInt(0);
		}
		return 0;
	}

}
