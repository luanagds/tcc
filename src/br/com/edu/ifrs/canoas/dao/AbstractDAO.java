package br.com.edu.ifrs.canoas.dao;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class AbstractDAO <TIPO>{
	protected SQLiteDatabase db;
	
	public void iniciaDB(Context t){
		BDHelper bdHelper = new BDHelper(t);
		db = bdHelper.getWritableDatabase();		
	}
	
	public abstract boolean create(TIPO obj);
	
	public abstract int update(TIPO obj);
	
	public abstract int remove(TIPO obj);
		
	public abstract List<TIPO> findAll();
	
}
