package br.com.edu.ifrs.canoas.dao;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.edu.ifrs.canoas.model.Despesa;
import br.com.edu.ifrs.canoas.model.ListaRegistro;
import br.com.edu.ifrs.canoas.model.Registro;

public class RegistroDAO extends AbstractDAO<Registro>{

	public RegistroDAO(Context t){
		iniciaDB(t);
	}
	
	@Override
	public boolean create(Registro obj) {
		ContentValues cv= new ContentValues();		
		cv.put("valor", obj.getValor());
		cv.put("observacao", obj.getObservacao());
		cv.put("tipo", obj.getTipo());
		cv.put("cat", obj.getIdcat());
		cv.put("data", String.valueOf(obj.getData().toString()));
		db.insert("registro", null, cv);
		Log.v("REG:", obj.toString());
		return true;
	}
		
	@Override
	public int update(Registro obj) {
		ContentValues cv= new ContentValues();		
		cv.put("valor", obj.getValor());
		cv.put("observacao", obj.getObservacao());
		cv.put("tipo", obj.getTipo());
		cv.put("cat", obj.getIdcat());
		cv.put("data", String.valueOf(obj.getData().toString()));
		return db.update("registro", cv, "_id = ?", new String[]{""+obj.getId()});		 
	}

	@Override
	public int remove(Registro obj) {
		return db.delete("registro", "_id = "+obj.getId(), null);
	}

	@Override
	public List<Registro> findAll() {
		ListaRegistro lista = new ListaRegistro();
		//List<Registro> lista = new ArrayList<Registro();
		//String[] campos = {"CATEGORIA.descricao","valor","observacao","data"};
		String[] campos = {"_id","valor","observacao","data", "cat", "tipo"};
		/*Cursor c = db.query("REGISTRO, CATEGORIA",
				campos, "registro.cat = categoria._id", null, null, null,"data ASC");	
			*/		
		Cursor c = db.query("REGISTRO",
				campos, null, null, null, null,"data ASC");	
			
		if(c.moveToFirst()){
			do{	
				Registro r = new Despesa();
				//r.setDescCat(c.getString(0));
				r.setId(c.getInt(0));
				r.setValor(c.getDouble(1));
				r.setObservacao(c.getString(2));		
				r.setData(Date.valueOf(c.getString(3)));
				r.setIdcat(c.getInt(4));
				r.setTipo(c.getInt(5));
				lista.add(r);

			}while(c.moveToNext());
		}			

		Log.v("REGISTRO", lista.toString());
		return lista;	
		
	}
	
	public List<Registro> findReg(long id, int mes){
		ListaRegistro lista = new ListaRegistro();
		Calendar calendar = Calendar.getInstance(); 
		int ano = calendar.get(Calendar.YEAR); 
		String[] campos = {"REGISTRO._id","valor","observacao","cat", "data", "CATEGORIA.descricao", "tipo"};
		Cursor c = db.query("REGISTRO, CATEGORIA",
				campos, "registro.cat = categoria._id and registro.tipo =  "+id+" and strftime('%m', data) ='"+mes+"' and strftime('%Y', data) ='"+ano+"'", null, null, null,"data ASC");	
					
		if(c.moveToFirst()){
			do{		
				Registro r = new Despesa();
				r.setId(c.getInt(0));
				r.setValor(c.getDouble(1));
				r.setObservacao(c.getString(2));
				r.setIdcat(c.getInt(3));				
				r.setData(Date.valueOf(c.getString(4)));
				r.setDescCat(c.getString(5));
				r.setTipo(c.getInt(6));
				lista.add(r);
			}while(c.moveToNext());
		}			
		
		return lista;
	}
		
	public String buscaDesp(int mes, int ano){
		double totalDesp = 0;	
		
		Cursor c = db.rawQuery("select sum(valor) from registro where tipo = 1"
				+ " and strftime('%m', data) = '"+mes+"' group by strftime('%Y', data) = '"+ano+"'", null);

		if(c.moveToFirst()){				
				totalDesp = c.getDouble(0);
		}
		DecimalFormat df = new DecimalFormat("0.00");
        String dx = df.format(totalDesp);
		
		return dx;
	}
	
	public String buscaRec(int mes, int ano){
		double totalRec = 0;	

		Cursor c = db.rawQuery("select sum(valor) from registro where tipo = 2"
				+ " and strftime('%m', data) = '"+mes+"' and strftime('%Y', data) = '"+ano+"'", null);

		if(c.moveToFirst()){				
				totalRec = c.getDouble(0);
		}
		
		DecimalFormat df = new DecimalFormat("0.00");
        String dx = df.format(totalRec);
		
		return(dx);
	}
	
	public String buscaTotal(int mes, int ano){
		double total = 0;	
		Cursor c = db.rawQuery("select sum(valor) from registro "
				+ "where strftime('%m', data) = '"+mes+"' and strftime('%Y', data) = '"+ano+"'", null);

		if(c.moveToFirst()){				
			total = c.getDouble(0);
		}
		DecimalFormat df = new DecimalFormat("0.00");
        String dx = df.format(total);
		return(dx);
	}
	
	public String buscaTotalAno(int ano){
		double total = 0;	
		
		Cursor c = db.rawQuery("select sum(valor) from registro where strftime('%Y', data) = '"+ano+"'", null);
		if(c.moveToFirst()){				
			total = c.getDouble(0);
		}

		DecimalFormat df = new DecimalFormat("0.00");
        String dx = df.format(total);
		
		return(dx);
	}

	public List<String> buscaCategReg(int tipo) {
		List<String> lista = new ArrayList<String>();
		
		String[] campos = {"CATEGORIA.descricao"};
		Cursor c = db.query("REGISTRO, CATEGORIA",
				campos, "registro.cat = categoria._id and registro.tipo = "+tipo, null, "CATEGORIA.descricao", null,null);	
		
		if(c.moveToFirst()){
			do{		
				String desc = c.getString(0);
				lista.add(desc);
			}while(c.moveToNext());
		}				
		return lista;		
	}

	public double buscaTodosReg(String cat, int tipo) {
		double val = 0;
		String[] campos = {"sum(valor)"};
		Cursor c = db.query("REGISTRO, CATEGORIA",
				campos, "registro.cat = categoria._id and registro.tipo = "+tipo+" and categoria.descricao = '"+cat+"'", null, null, null,null);	
		
		if(c.moveToFirst()){
			do{		
				val = c.getDouble(0);
			}while(c.moveToNext());
		}				
		Log.v("findDesp:", String.valueOf(val));
		String ret = String.valueOf(val);
		String ret2 = ret.replaceAll("-", "");
		val = Double.valueOf(ret2);
		return val;		
	}

	public double buscaTotReg(int ano, int tipo) {
		double total = 0;	
		
		Cursor c = db.rawQuery("select sum(valor) from registro where strftime('%Y', data) = '"+ano+"' and tipo ="+tipo, null);
		if(c.moveToFirst()){				
			total = c.getDouble(0);
		}
		return total;
	}

	public List<Registro> findRegAn(long id, int ano) {
		ListaRegistro lista = new ListaRegistro();
		
		String[] campos = {"REGISTRO._id","valor","observacao","cat", "data", "CATEGORIA.descricao", "tipo"};
		Cursor c = db.query("REGISTRO, CATEGORIA",
				campos, "registro.cat = categoria._id and registro.tipo =  "+id+" and strftime('%Y', data) ='"+ano+"'", null, null, null,"data ASC");	
					
		if(c.moveToFirst()){
			do{		
				Registro r = new Despesa();
				r.setId(c.getInt(0));
				r.setValor(c.getDouble(1));
				r.setObservacao(c.getString(2));
				r.setIdcat(c.getInt(3));				
				r.setData(Date.valueOf(c.getString(4)));
				r.setDescCat(c.getString(5));
				r.setTipo(c.getInt(6));
				lista.add(r);
			}while(c.moveToNext());
		}				
		return lista;
	}
	
}
