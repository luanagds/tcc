package br.com.edu.ifrs.canoas.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.edu.ifrs.canoas.model.Categoria;

public class CategoriaDAO extends AbstractDAO<Categoria>{

	public CategoriaDAO(Context t){
		iniciaDB(t);
	}
	
	@Override
	public boolean create(Categoria obj) {
		ContentValues cv = new ContentValues();
		cv.put("descricao", obj.getDescricao());
		cv.put("despesa", obj.getDesp());
		cv.put("receita", obj.getRec());
		db.insert("categoria", null, cv);
		return true;
	}

	@Override
	public int update(Categoria obj) {
		ContentValues cv = new ContentValues();
		cv.put("descricao", obj.getDescricao());
		return db.update("categoria", cv, "_id = ?", new String[]{""+obj.getId()});
	}

	@Override
	public int remove(Categoria obj) {
		return db.delete("categoria", "_id = "+obj.getId(), null);
	}
	
	
	@Override
	public List<Categoria> findAll() {
		List<Categoria>lista = new ArrayList<Categoria>();
		String[] campos= {"_id","descricao", "despesa", "receita"};			
		Cursor c = db.query("CATEGORIA", campos, null, 
										null, null, 
										null, "descricao ASC");			
		if(c.moveToFirst()){
			do{
				Categoria cat = new Categoria();
				cat.setId(c.getInt(0));				
				cat.setDescricao(c.getString(1));
				cat.setDesp(c.getInt(2));
				cat.setRec(c.getInt(3));
				lista.add(cat);
			}while(c.moveToNext());
			
		}	
		Log.v("CATEG", lista.toString());
		Log.v("classe", getClass().getCanonicalName());
		return lista;		
	}
	public List<Categoria> findCategDes() {
		List<Categoria>lista = new ArrayList<Categoria>();
		String[] campos= {"_id","descricao", "despesa", "receita"};			
		Cursor c = db.query("CATEGORIA", campos, "despesa=1", 
										null, null, 
										null, "descricao ASC");			
		if(c.moveToFirst()){
			do{
				Categoria cat = new Categoria();
				cat.setId(c.getInt(0));				
				cat.setDescricao(c.getString(1));
				cat.setDesp(c.getInt(2));
				cat.setRec(c.getInt(3));
				lista.add(cat);
			}while(c.moveToNext());
			
		}	
		Log.v("CATEG", lista.toString());
		Log.v("classe", getClass().getCanonicalName());
		return lista;		
	}
	
	public List<Categoria> findCategRec() {
		List<Categoria>lista = new ArrayList<Categoria>();
		String[] campos= {"_id","descricao", "despesa", "receita"};			
		Cursor c = db.query("CATEGORIA", campos, "receita=1", 
										null, null, 
										null, "descricao ASC");			
		if(c.moveToFirst()){
			do{
				Categoria cat = new Categoria();
				cat.setId(c.getInt(0));				
				cat.setDescricao(c.getString(1));
				cat.setDesp(c.getInt(2));
				cat.setRec(c.getInt(3));
				lista.add(cat);
			}while(c.moveToNext());
			
		}	
		Log.v("CATEG", lista.toString());
		Log.v("classe", getClass().getCanonicalName());
		return lista;		
	}
	
	
	public int findCategoria(String desc){
		String[] campos= {"count(descricao)"};	
		int cat = 0;
		Cursor c = db.query("CATEGORIA", campos, "descricao = '"+desc+"'", 
										null, null, 
										null, "descricao ASC");			
		if(c.moveToFirst()){			
				cat = c.getInt(0);							
		}			
		return cat;
	}

}
